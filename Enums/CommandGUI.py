from enum import Enum

class CommandGUI(Enum):
    DriveForward = 0
    DriveBack = 1
    TurnLeft = 2
    TurnRight = 3
    FlyUp = 4
    FlyDown = 5
    FlyLeft = 6
    FlyRight = 7
    FlyForward = 8
    FlyBackward = 9
    Stop = 10
    Emergency = 11
    Test = 12
    FlyHover = 13
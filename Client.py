import threading
import queue
from tkinter import *
import socket
import time

from Enums import CommandGUI


class Client:
    def __init__(self, hostname=None):
        if hostname is None:
            self.hostname = socket.gethostname()
        else:
            self.hostname = hostname
        self.port = 5000;

        self.parent = 0

        self.connectionQueue = queue.Queue()
        threading.Thread(target=self.run).start()
        threading.Thread(target=self.checkConnection).start()

    def run(self):
        self.mainWindow = Frame(self.parent)

        fOuter = Frame(self.mainWindow, border=2, relief="sunken")
        fButtons = Frame(fOuter, border=2, relief="raised")

        buttonWitdth = 10
        buttonHeigh = 3

        bEmergency = Button(fButtons, text="Emergency", width=buttonWitdth, height=buttonHeigh, command=self.emergency)
        bEmergency.grid(row=0, column=0)

        bStop = Button(fButtons, text="Stop", width=buttonWitdth, height=buttonHeigh, command=self.stop)
        bStop.grid(row=0, column=1)

        bFlyHover = Button(fButtons, text="FlyHover", width=buttonWitdth, height=buttonHeigh, command=self.flyHover)
        bFlyHover.grid(row=0, column=2)

        self.lConnection = Label(fButtons, text="Connection", foreground="red")
        self.lConnection.grid(row=0, column=5)

        bFlyUp = Button(fButtons, text="FlyUp", width=buttonWitdth, height=buttonHeigh, command=self.flyUp)
        bFlyUp.grid(row=1, column=0)

        bFlyDown = Button(fButtons, text="FlyDown", width=buttonWitdth, height=buttonHeigh, command=self.flyDown)
        bFlyDown.grid(row=1, column=1)

        bFlyForward = Button(fButtons, text="FlyForward", width=buttonWitdth, height=buttonHeigh, command=self.flyForward)
        bFlyForward.grid(row=1, column=2)

        bFlyBack = Button(fButtons, text="FlyBack", width=buttonWitdth, height=buttonHeigh, command=self.flyBack)
        bFlyBack.grid(row=1, column=3)

        bFlyLeft = Button(fButtons, text="FlyLeft", width=buttonWitdth, height=buttonHeigh, command=self.flyLeft)
        bFlyLeft.grid(row=1, column=4)

        bFlyRight = Button(fButtons, text="FlyRight", width=buttonWitdth, height=buttonHeigh, command=self.flyRight)
        bFlyRight.grid(row=1, column=5)

        bDriveForward = Button(fButtons, text="DriveForward", width=buttonWitdth, height=buttonHeigh, command=self.driveForward)
        bDriveForward.grid(row=2, column=0)

        bDriveBack = Button(fButtons, text="DriveBack", width=buttonWitdth, height=buttonHeigh, command=self.driveBack)
        bDriveBack.grid(row=2, column=1)

        bTurnLeft = Button(fButtons, text="TurnLeft", width=buttonWitdth, height=buttonHeigh, command=self.turnLeft)
        bTurnLeft.grid(row=2, column=2)

        bTurnRight = Button(fButtons, text="TurnRight", width=buttonWitdth, height=buttonHeigh, command=self.turnRight)
        bTurnRight.grid(row=2, column=3)

        fButtons.pack(fill=X)
        fOuter.pack(fill=X)
        self.mainWindow.pack()

        self.mainWindow.master.title("The Hybrid")

        self.mainWindow.after(100, self.listen_for_result)
        self.mainWindow.mainloop()

    def emergency(self):
        self.sendCommand(CommandGUI.CommandGUI.Emergency)

    def stop(self):
        self.sendCommand(CommandGUI.CommandGUI.Stop)

    def flyForward(self):
        self.sendCommand(CommandGUI.CommandGUI.FlyForward)

    def flyBack(self):
        self.sendCommand(CommandGUI.CommandGUI.FlyBackward)

    def flyUp(self):
        self.sendCommand(CommandGUI.CommandGUI.FlyUp)

    def flyDown(self):
        self.sendCommand(CommandGUI.CommandGUI.FlyDown)

    def flyLeft(self):
        self.sendCommand(CommandGUI.CommandGUI.FlyLeft)

    def flyRight(self):
        self.sendCommand(CommandGUI.CommandGUI.FlyRight)

    def flyHover(self):
        self.sendCommand(CommandGUI.CommandGUI.FlyHover)

    def driveForward(self):
        self.sendCommand(CommandGUI.CommandGUI.DriveForward)

    def driveBack(self):
        self.sendCommand(CommandGUI.CommandGUI.DriveBack)

    def turnLeft(self):
        self.sendCommand(CommandGUI.CommandGUI.TurnLeft)

    def turnRight(self):
        self.sendCommand(CommandGUI.CommandGUI.TurnRight)

    def sendCommand(self, command):
        client_socket = socket.socket()  # instantiate
        client_socket.connect((self.hostname, self.port))  # connect to the server
        message = str(command.value)
        client_socket.send(message.encode())
        client_socket.close()

    def listen_for_result(self):
        try:
            res = self.connectionQueue.get(0)
            if res:
                self.lConnection.config(foreground="green")
            else:
                self.lConnection.config(foreground="red")

            self.mainWindow.after(100, self.listen_for_result)

        except queue.Empty:
            self.mainWindow.after(100, self.listen_for_result)

    def checkConnection(self):
        while True:
            try:
                client_socket = socket.socket()  # instantiate
                client_socket.connect((self.hostname, self.port))  # connect to the server
                message = str(CommandGUI.CommandGUI.Test.value)
                client_socket.send(message.encode())
                client_socket.close()
            except ConnectionRefusedError:
                self.connectionQueue.put(False)
            else:
                self.connectionQueue.put(True)

            time.sleep(1)





